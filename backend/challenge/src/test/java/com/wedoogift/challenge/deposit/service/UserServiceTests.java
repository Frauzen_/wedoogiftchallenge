package com.wedoogift.challenge.deposit.service;

import com.wedoogift.challenge.deposit.model.*;
import com.wedoogift.challenge.deposit.repository.UserRepository;
import com.wedoogift.challenge.deposit.repository.WalletRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {
    final Logger LOGGER = LoggerFactory.getLogger(UserServiceTests.class);

    @Mock
    UserRepository userRepository;

    @Mock
    WalletRepository walletRepository;

    @InjectMocks
    UserService userService;

    @Mock
    Employee employee;

    @Mock
    Wallet wallet;

    @Test
    // Test deposit with the right parameters
    public void greatDeposit() {
        long idCompany = 1L;
        long idEmployee = 2L;
        long idWallet = 1L;
        int amount = 100;

        // We don’t mock the company to test its balance
        Company company = new Company();
        company.setBalance(1000);

        Mockito.when(walletRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(wallet));
        Mockito.when(userRepository.findById(idCompany)).thenReturn(Optional.of(company));
        Mockito.when(userRepository.findById(idEmployee)).thenReturn(Optional.ofNullable(employee));

        // Gift part
        Deposit gift = null;
        try {
            gift = makeDeposit(idCompany, idEmployee, idWallet, amount, WalletType.GIFT);
        } catch (Exception e) {
            LOGGER.error("An exception occurred during deposit", e);
        }
        Assert.notNull(gift, "Deposit must be not null");
        assertEmployeeBalance(gift, idWallet, amount);
        assertCompanyBalance(gift, amount);

        // Meal part
        idWallet = 2L;
        Deposit meal = null;
        try {
            meal = makeDeposit(idCompany, idEmployee, idWallet, amount, WalletType.MEAL);
        } catch (Exception e) {
            LOGGER.error("An exception occurred during deposit", e);
        }
        Assert.notNull(meal, "Deposit must be not null");
        assertEmployeeBalance(meal, idWallet, amount);
        //amount * 2 because we are deleting the amount twice
        assertCompanyBalance(meal, amount * 2);
    }

    @Test
    // Test deposit with the wrong parameters
    public void badDeposit() {
        long idCompany = 1L;
        long idEmployee = 2L;
        Long idWallet = 1L;
        int amount = 100;
        String exceptionMessage = "";

        // Wallet part: no database match
        Mockito.when(walletRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        try {
            makeDeposit(idCompany, idEmployee, idWallet, amount, WalletType.GIFT);
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
        }
        Assert.hasText("Wallet not found", exceptionMessage);
        Mockito.when(walletRepository.findById(Mockito.anyLong())).thenReturn(Optional.ofNullable(wallet));

        // Company part: no database match
        Mockito.when(userRepository.findById(idCompany)).thenReturn(Optional.empty());
        try {
            makeDeposit(idCompany, idEmployee, idWallet, amount, WalletType.GIFT);
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
        }
        Assert.hasText("Company not found", exceptionMessage);
        Company company = Mockito.mock(Company.class);
        Mockito.when(company.getBalance()).thenReturn(0);
        Mockito.when(userRepository.findById(idCompany)).thenReturn(Optional.of(company));

        // Employee part: no database match
        Mockito.lenient().when(userRepository.findById(idEmployee)).thenReturn(Optional.empty());
        try {
            makeDeposit(idCompany, idEmployee, idWallet, amount, WalletType.GIFT);
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
        }
        Assert.hasText("Employee not found", exceptionMessage);

        // Company balance part: not enough money
        try {
            makeDeposit(idCompany, idEmployee, idWallet, amount, WalletType.GIFT);
        } catch (Exception e) {
            exceptionMessage = e.getMessage();
        }
        Assert.hasText("There's not enough money in the company.", exceptionMessage);

    }

    @Test
    // Test to calculate user's balance
    public void employeeBalance() {
        Long idEmployee = 1L;
        Long idWallet = 1L;
        int amount = 200;

        // We don’t mock because we want to be able to test the amount of the employee on different wallet
        EmployeeBalance employeeBalanceGift = new EmployeeBalance();
        Wallet walletGift = new Wallet();
        walletGift.setId(idWallet);
        employeeBalanceGift.setWallet(walletGift);
        employeeBalanceGift.setAmount(amount);

        EmployeeBalance employeeBalanceMeal = new EmployeeBalance();
        Wallet walletMeal = new Wallet();
        walletMeal.setId(idWallet + 1);
        employeeBalanceMeal.setWallet(walletMeal);
        employeeBalanceMeal.setAmount(amount * 2);

        List<EmployeeBalance> employeeBalanceList = Arrays.asList(employeeBalanceGift, employeeBalanceMeal);

        Mockito.when(userRepository.findById(idEmployee)).thenReturn(Optional.ofNullable(employee));
        Mockito.when(employee.getEmployeeBalanceList()).thenReturn(employeeBalanceList);

        int balanceForGift = userService.getEmployeeBalanceByWallet(idEmployee, idWallet);
        Assert.isTrue(balanceForGift == amount, "Employee balance for wallet " + idWallet + " must be " + amount);

        int balanceAll = userService.getAllEmployeeBalance(idEmployee);
        Assert.isTrue(balanceAll == (amount + (amount * 2)), "Employee balance for wallet " + idWallet + " must be " + (amount + (amount * 2)));
    }

    @Test
    // Test the expiration date of deposit
    public void expireDeposit() {
        // Gift part
        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH) - 365);
        Date depositDate = calendar.getTime();

        Deposit gift = new Gift();
        gift.setDepositDate(depositDate);
        Assert.isTrue(gift.isExpire(), "Gift deposit expiration date is set to 365 days");

        // Meal part
        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR) - 1, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        depositDate = calendar.getTime();

        Deposit meal = new Meal();
        meal.setDepositDate(depositDate);

        calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR), Calendar.FEBRUARY, 28);

        // If the current date is before 28/02 of the year, the expiry date is incorrect because the deposit date is 28/02 of the preceding year
        boolean isOk;
        if (new Date().before(calendar.getTime())) {
            isOk = !meal.isExpire();
        } else {
            isOk = meal.isExpire();
        }

        Assert.isTrue(isOk, "Meal deposit expiration date is set to +1 year at 02/28/yyyy");
    }

    /**
     * Make a deposit with some mock
     *
     * @param idCompany  Company id
     * @param idEmployee Employee id
     * @param idWallet   Wallet id
     * @param amount     Deposit amount
     * @param walletType Wallet type
     * @return The deposit or null
     * @throws Exception Wallet, Company or Employee does not exist or company does not have enough money
     */
    private Deposit makeDeposit(Long idCompany, Long idEmployee, Long idWallet, int amount, WalletType walletType) throws Exception {
        Mockito.lenient().when(wallet.getId()).thenReturn(idWallet);
        Mockito.lenient().when(wallet.getType()).thenReturn(walletType);

        // We don't mock the list because we want to be able to test the amount of the employee
        EmployeeBalance employeeBalance = new EmployeeBalance();
        employeeBalance.setWallet(wallet);
        employeeBalance.setAmount(0);
        List<EmployeeBalance> employeeBalanceList = Collections.singletonList(employeeBalance);

        Mockito.lenient().when(employee.getEmployeeBalanceList()).thenReturn(employeeBalanceList);

        return userService.makeDeposit(idCompany, idEmployee, idWallet, amount);
    }

    /**
     * Assert that employee balance by wallet is equal to the deposit amount
     *
     * @param deposit  Deposit
     * @param idWallet Wallet id
     * @param amount   Deposit amount
     */
    private void assertEmployeeBalance(Deposit deposit, Long idWallet, int amount) {
        boolean isOK = false;
        for (EmployeeBalance empBalance : ((Employee) deposit.getEmployee()).getEmployeeBalanceList()) {
            if (empBalance.getWallet().getId().equals(idWallet)) {
                isOK = empBalance.getAmount() == amount;
            }
        }
        Assert.isTrue(isOK, "Employee balance must be " + amount);
    }

    /**
     * Assert that company balance is equal to its balance less the deposit amount
     *
     * @param deposit Deposit
     * @param amount  Deposit amount
     */
    private void assertCompanyBalance(Deposit deposit, int amount) {
        Assert.isTrue(((Company) deposit.getCompany()).getBalance() == (1000 - amount), "Company balance must be " + (1000 - amount));
    }
}
