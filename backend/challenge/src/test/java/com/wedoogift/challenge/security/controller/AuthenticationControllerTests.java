package com.wedoogift.challenge.security.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wedoogift.challenge.deposit.repository.UserRepository;
import com.wedoogift.challenge.security.jwt.AuthEntryPointJwt;
import com.wedoogift.challenge.security.jwt.AuthTokenFilter;
import com.wedoogift.challenge.security.jwt.JwtUtils;
import com.wedoogift.challenge.security.payload.LoginRequest;
import com.wedoogift.challenge.security.service.UserDetailsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;

@WebMvcTest(value = AuthenticationController.class, includeFilters = {
        // to include JwtUtil in spring context
        @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = JwtUtils.class)})
public class AuthenticationControllerTests {
    private static UserDetails dummy;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AuthTokenFilter authTokenFilter;

    @MockBean
    AuthenticationManager authenticationManager;

    @MockBean
    JwtUtils jwtUtils;

    @MockBean
    UserDetailsServiceImpl userDetailsServiceImpl;

    @MockBean
    AuthEntryPointJwt authEntryPointJwt;

    @Mock
    UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        // the password is 'password' encoded
        dummy = new org.springframework.security.core.userdetails.User("john.cena@gmail.com", "$2a$10$cnkrKCiR515koZ.nK3/YPeIgVSpCsgTIupQjX76JPMVx8JoLzVita", new ArrayList<>());
    }

    @Test
    // Not working, see README.md
    public void login() {
        String uri = "http://localhost:8080/api/authentication/signing";
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setUsername("John");
        loginRequest.setPassword("Cena");

        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String inputJson = objectMapper.writeValueAsString(loginRequest);

            RequestBuilder request = MockMvcRequestBuilders
                    .post(uri)
                    .content(inputJson)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON);

            Mockito.when(userDetailsServiceImpl.loadUserByUsername(Mockito.anyString())).thenReturn(dummy);

            MvcResult mvcResult = mockMvc.perform(request).andReturn();

            String result = mvcResult.getResponse().getContentAsString();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
