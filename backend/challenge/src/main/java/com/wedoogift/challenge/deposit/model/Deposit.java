package com.wedoogift.challenge.deposit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
public abstract class Deposit {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    private Wallet wallet;

    @ManyToOne
    private User company;

    @ManyToOne
    private User employee;

    private Date depositDate;
    private Date expireDate;
    private int amount;

    /**
     * Check if the deposit has expired
     *
     * @return true if the expiry date is before today
     */
    public boolean isExpire() {
        return getExpireDate().before(new Date());
    }
}