package com.wedoogift.challenge.deposit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class EmployeeBalance {
    private Wallet wallet;
    private int amount;
}
