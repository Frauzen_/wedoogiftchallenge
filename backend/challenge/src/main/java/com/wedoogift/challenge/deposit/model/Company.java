package com.wedoogift.challenge.deposit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Company extends User {
    private int balance;
}
