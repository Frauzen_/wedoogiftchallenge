package com.wedoogift.challenge.deposit.model;

public enum WalletType {
    GIFT,
    MEAL
}
