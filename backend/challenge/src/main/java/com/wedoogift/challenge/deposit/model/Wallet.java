package com.wedoogift.challenge.deposit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class Wallet {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private WalletType type;
}
