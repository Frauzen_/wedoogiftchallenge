package com.wedoogift.challenge.deposit.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class Employee extends User {
    List<EmployeeBalance> employeeBalanceList = new ArrayList<>();
}
