package com.wedoogift.challenge.deposit.controller;

import com.wedoogift.challenge.deposit.model.Deposit;
import com.wedoogift.challenge.deposit.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/deposit/{idCompany}/{idEmployee}/{idWallet}/{amount}")
    @PreAuthorize("hasRole('ROLE_COMPANY')")
    public Deposit makeDeposit(@PathVariable Long idCompany, @PathVariable Long idEmployee, @PathVariable Long idWallet, @PathVariable int amount) throws Exception {
        LOGGER.trace("Company makes a deposit - idCompany: " + idCompany + ", idEmployee: " + idEmployee + ", idWallet: " + idWallet + ", amount: " + amount);
        return userService.makeDeposit(idCompany, idEmployee, idWallet, amount);
    }

    @GetMapping("/balance/by-wallet/{idEmployee}/{idWallet}")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public int getEmployeeBalanceByWallet(@PathVariable Long idEmployee, @PathVariable Long idWallet) {
        LOGGER.trace("Get employee balance by wallet - idEmployee: " + idEmployee + ", idWallet: " + idWallet);
        return userService.getEmployeeBalanceByWallet(idEmployee, idWallet);
    }

    @GetMapping("/balance/find-all/{idEmployee}")
    @PreAuthorize("hasRole('ROLE_EMPLOYEE')")
    public int getEmployeeBalanceByWallet(@PathVariable Long idEmployee) {
        LOGGER.trace("Get all employee balance - idEmployee: " + idEmployee);
        return userService.getAllEmployeeBalance(idEmployee);
    }
}