package com.wedoogift.challenge.deposit.model;

import java.util.Calendar;
import java.util.Date;

public class Gift extends Deposit {
    private static final int EXPIRE_IN = 365;

    // For gift, the expiration date is set to 365 days
    @Override
    public Date getExpireDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDepositDate());
        calendar.add(Calendar.DAY_OF_MONTH, EXPIRE_IN - 1);
        return calendar.getTime();
    }
}
