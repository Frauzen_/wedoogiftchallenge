package com.wedoogift.challenge.deposit.service;

import com.wedoogift.challenge.deposit.model.*;
import com.wedoogift.challenge.deposit.repository.UserRepository;
import com.wedoogift.challenge.deposit.repository.WalletRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);
    private UserRepository userRepository;
    private WalletRepository walletRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setWalletRepository(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    /**
     * Make a deposit
     *
     * @param idCompany  Company id
     * @param idEmployee Employee id
     * @param idWallet   Wallet id
     * @param amount     Deposit amount
     * @return The deposit or null
     * @throws Exception Wallet, Company or Employee does not exist or company does not have enough money
     */
    public Deposit makeDeposit(Long idCompany, Long idEmployee, Long idWallet, int amount) throws Exception {
        Deposit deposit;
        Wallet wallet = walletRepository.findById(idWallet).orElseThrow(() -> new EntityNotFoundException("Wallet not found"));
        // Deposit type is the same as wallet
        if (wallet.getType().equals(WalletType.GIFT)) {
            deposit = new Gift();
        } else {
            deposit = new Meal();
        }
        LOGGER.trace("Wallet type: " + wallet.getType());

        Company company = (Company) userRepository.findById(idCompany).orElseThrow(() -> new EntityNotFoundException("Company not found"));
        LOGGER.trace("Company balance: " + company.getBalance());
        if (company.getBalance() >= amount) {
            company.setBalance(company.getBalance() - amount);

            // Set the employee amount for the right wallet
            Employee employee = (Employee) userRepository.findById(idEmployee).orElseThrow(() -> new EntityNotFoundException("Employee not found"));
            for (EmployeeBalance employeeBalance : employee.getEmployeeBalanceList()) {
                if (employeeBalance.getWallet().getId().equals(wallet.getId())) {
                    employeeBalance.setAmount(employeeBalance.getAmount() + amount);
                    LOGGER.trace("New employee balance amount: " + employeeBalance.getAmount());
                }
            }

            deposit.setWallet(wallet);
            deposit.setCompany(company);
            deposit.setEmployee(employee);
            deposit.setDepositDate(new Date());
            deposit.setAmount(amount);
        } else {
            throw new Exception("There's not enough money in the company.");
        }

        return deposit;
    }

    /**
     * Calculate the employee's balance based on their wallet
     *
     * @param idEmployee Employee id
     * @param idWallet   Wallet id
     * @return The employee's balance
     */
    public int getEmployeeBalanceByWallet(Long idEmployee, Long idWallet) {
        int balance = 0;
        Employee employee = (Employee) userRepository.findById(idEmployee).orElseThrow(() -> new EntityNotFoundException("Employee not found"));
        for (EmployeeBalance employeeBalance : employee.getEmployeeBalanceList()) {
            if (employeeBalance.getWallet().getId().equals(idWallet)) {
                balance = employeeBalance.getAmount();
                LOGGER.trace("Employee balance for wallet (id: " + idWallet + ") : " + balance);
            }
        }

        return balance;
    }

    /**
     * Calculate the employee's balance based on all these wallets
     *
     * @param idEmployee Employee id
     * @return All employee balances added together
     */
    public int getAllEmployeeBalance(Long idEmployee) {
        int balance = 0;
        Employee employee = (Employee) userRepository.findById(idEmployee).orElseThrow(() -> new EntityNotFoundException("Employee not found"));
        for (EmployeeBalance employeeBalance : employee.getEmployeeBalanceList()) {
            balance += employeeBalance.getAmount();
            LOGGER.trace("All employee balances : " + balance);
        }

        return balance;
    }
}
