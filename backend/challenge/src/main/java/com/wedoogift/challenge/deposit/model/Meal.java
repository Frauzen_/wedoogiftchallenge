package com.wedoogift.challenge.deposit.model;

import java.util.Calendar;
import java.util.Date;

public class Meal extends Deposit {
    private final static int EXPIRE_YEAR = 1;
    private final static int EXPIRE_MONTH = Calendar.FEBRUARY;
    private final static int EXPIRE_DAY = 28;

    // For meal, the expiration date is set to +1 year at 02/28/yyyy
    @Override
    public Date getExpireDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(getDepositDate());
        // Expiry year must be greater than deposit year
        calendar.set(calendar.get(Calendar.YEAR) + EXPIRE_YEAR, EXPIRE_MONTH, EXPIRE_DAY);
        return calendar.getTime();
    }
}
