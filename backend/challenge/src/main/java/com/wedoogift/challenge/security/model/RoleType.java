package com.wedoogift.challenge.security.model;

public enum RoleType {
    ROLE_COMPANY,
    ROLE_EMPLOYEE
}
