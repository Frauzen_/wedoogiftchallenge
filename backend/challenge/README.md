
# Wedoogift whallenge

## Introduction

Bonjour, je m'appel Luc Rodière et j'ai effectué le test backend avec Spring Boot (open-jdk 17).

## Dépôt - package deposit

* Les entités sont codées de telle sorte à ce quels puissent être connecté à une base de données (@Entity, @Id, @GeneratedValue, @ManyToOne...).
* Les classes Deposit et User sont des classes abstraites:
    * Deposit: Gift et Meal
    * User: Company et Employee
* La classe UserService est la classe qui met en oeuvre tous les traitements des données:
    * La classe de test UserServiceTests est à votre disposition pour vérifier son bon fonctionnement.
    * Les tests se font via des mocks.

## Bonus - package security

Mise en place d'une api REST + Spring Security + JWT:
* La classe UserController est le point d'accès aux données via http://localhost:8080/api/user/
    * Il faut disposer d'un utilisateur avec les bons droits
* La classe AuthenticationController permet de s'authentifier via http://localhost:8080/api/authentication/
* Une classe de test est disponible pour la sécurité mais n'est pas fonctionnelle (AuthenticationControllerTests).
    * Malgré cela pour tester cette partie, il suffit de remplacer la ligne avec le commentaire //ICI dans la classe UserDetailsServiceImpl par:
      <br>User user = new Employee();
      <br>user.setId(1L);
      <br>user.setName("toto");
      <br>user.setUsername("toto");
      <br>user.setPassword("$2a$10$75ydvfgPjY1vvGEkd0bY/ewwXtae75KzSQ6RZYhyFKzd8.pkQcrdq");
      <br>Role role = new Role();
      <br>role.setId(1L);
      <br>role.setName(RoleType.ROLE_EMPLOYEE);
      <br>user.setRoles(new HashSet<Role>(Collections.singletonList(role)));
    * Avec Postman: POST | localhost:8080/api/authentication/signing | body raw JSON: {"username": "toto","password": "toto"}
    * GET | localhost:8080/api/user/balance/by-wallet/1/1 | header: Authorization: Bearer 'jwtFromPreviousRequest'
        * On rentre bien dans la classe UserController puis on récupère une erreur car les ids fourni ne correspondent à rien.
    * GET | localhost:8080/api/user/deposit/1/1/1/200 | header: Authorization: Bearer 'jwtFromPreviousRequest'
        * La requête renvoie vide car l'utilisateur n'a pas les droits d'accès.

## Git

Je n'ai pas compris ce que signifiais un commit par niveau, j'ai donc effectué un commit par package 'père':
* src/main/java/deposit
* src/main/java/security
* src/main/test/
* le reste